from django.urls import path
from .views import list_project, project_view, create_project

urlpatterns = [
    path("", list_project, name="list_projects"),
    path("<int:id>/", project_view, name="show_project"),
    path("create/", create_project, name="create_project"),
]
